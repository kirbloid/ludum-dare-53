extends Node2D

enum CARGO { EMPTY, RED, BLUE, GREEN }

var cargo_holder = []

func add_cargo(cargo_type: CARGO):
	if $Cargo.get_child_count() > cargo_holder.size():
		cargo_holder.append(cargo_type)
		update_cargo()


func remove_cargo():
	if $Cargo.get_child_count() > 0:
		cargo_holder.remove_at(0)
		update_cargo()


func get_next_cargo():
	if cargo_holder.size() > 0:
		return cargo_holder[0]
	else:
		return CARGO.EMPTY


func update_cargo():
	var slot = 0
	for cargo in $Cargo.get_children():
		cargo.current_cargo = CARGO.EMPTY
	for cargo in cargo_holder:
		$Cargo.get_child(slot).current_cargo = cargo
		slot += 1


func toggle_rocket_trail(toggle: bool):
	$AnimatedSprite2D.play("default")
	$AnimatedSprite2D.set_visible(toggle)
