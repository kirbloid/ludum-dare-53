extends Sprite2D

enum CARGO { EMPTY, RED, BLUE, GREEN }

@export_range(CARGO.EMPTY, CARGO.GREEN, 1) var current_cargo = CARGO.EMPTY
@export var base_hold := false


func _ready():
	if base_hold:
		$BaseHold.set_visible(true)


func _process(delta):
	match current_cargo:
		CARGO.EMPTY:
			$GemRed.set_visible(false)
			$GemBlue.set_visible(false)
			$GemGreen.set_visible(false)
			$GemEmpty.set_visible(true)
		CARGO.RED:
			$GemRed.set_visible(true)
			$GemBlue.set_visible(false)
			$GemGreen.set_visible(false)
			$GemEmpty.set_visible(false)
		CARGO.BLUE:
			$GemRed.set_visible(false)
			$GemBlue.set_visible(true)
			$GemGreen.set_visible(false)
			$GemEmpty.set_visible(false)
		CARGO.GREEN:
			$GemRed.set_visible(false)
			$GemBlue.set_visible(false)
			$GemGreen.set_visible(true)
			$GemEmpty.set_visible(false)
