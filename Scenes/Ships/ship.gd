extends RigidBody2D

@onready var game = get_node("/root/Main/Game")
@onready var main = get_node("/root/Main")

var exploding = false

func launch(launch_speed: float):
	var launch_direction = transform.basis_xform(Vector2.UP).normalized()
	
	var launch_vector = launch_direction * launch_speed
	apply_impulse(Vector2(launch_vector))
	$AnimatedSprite2D.set_visible(true)
	$AnimatedSprite2D.play("default")
	var random_pitch = main.rng.randf_range(-0.25, 0.25)
	$TakeoffSFX.pitch_scale += random_pitch
	$TakeoffSFX.play()


func explode():
	for piece in get_children():
		piece.set_visible(false)
	$Explosion.set_visible(true)
	$Explosion.play("default")
	var random_pitch = main.rng.randf_range(-0.25, 0.25)
	$ExplosionSFX.pitch_scale += random_pitch
	$ExplosionSFX.play()


func _on_body_entered(body: StaticBody2D):
	$AnimatedSprite2D.set_visible(false)
	$TakeoffSFX.stop()
	if body.collision_layer == 1:
		if body.awaiting_delivery and not exploding:
			game.delivery_made(body)
		
		var random_pitch = main.rng.randf_range(-0.25, 0.25)
		$BounceSFX.pitch_scale += random_pitch
		$BounceSFX.play()
		game.start_countdown()
	elif body.collision_layer == 2:
		explode()

func _on_explosion_animation_finished():
	queue_free()
