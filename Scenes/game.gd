extends Node2D

# Hard coding levels because we don't have time for this
var levels = [
	preload("res://Scenes/Levels/level_1.tscn"),
	preload("res://Scenes/Levels/level_2.tscn"),
	preload("res://Scenes/Levels/level_3.tscn"),
	preload("res://Scenes/Levels/level_4.tscn"),
	preload("res://Scenes/Levels/level_5.tscn"),
	preload("res://Scenes/Levels/level_6.tscn"),
	preload("res://Scenes/Levels/level_7.tscn")
]
var current_level = 0
const MAX_ATTEMPTS = 9
var attempts = 0
var combo = 0

var loaded_level

enum CARGO { EMPTY, RED, BLUE, GREEN }

@onready var main := get_node("/root/Main")
@onready var aimer := get_node("Aimer/Pivot")
@onready var ship_launch_position := get_node("Aimer/Pivot/ShipPosition")
@onready var cargo_ui := get_node("UI/Rocket/RocketUI")

@onready var level_holder := get_node("LevelHolder")

# Scoring
@onready var score_text := get_node("UI/ScoreNumber")
@onready var delivery_text := preload("res://Scenes/delivery_text.tscn")

var ship
var thrust := 750
var launched := false


func _ready():
	main.reset_score()
	update_score()
	load_next_level()


func _process(delta):
	$UI/ShipCountdown.text = str($UI/Countdown/Timer.time_left).pad_decimals(3)
	$UI/Countdown.text = str($UI/Countdown/Timer.time_left).pad_decimals(3)
	if ship != null:
		$UI/ShipCountdown.position = ship.global_position
		
		if not launched:
			if cargo_ui.get_next_cargo() == CARGO.EMPTY:
				load_next_level()
			ship.position = ship_launch_position.global_position
			ship.rotation = aimer.global_rotation
			var aim_strength = Input.get_action_strength("aim_left") - Input.get_action_strength("aim_right")
			if Input.is_action_pressed("go_faster"):
				aim_strength *= 3
			aimer.rotation -= aim_strength * delta
			if Input.is_action_just_pressed("action_select") and aim_strength == 0:
				launched = true
				attempts -= 1
				$UI/AttemptsBG/Attempts.text = str(attempts)
				$UI/LaunchPad.set_visible(false)
				ship.launch(thrust)
				cargo_ui.toggle_rocket_trail(true)
		if Input.is_action_just_pressed("action_cancel") and launched:
			ship.explode()
	elif ship == null and attempts_left():
		setup_ship()
	else:
		load_next_level()


func delivery_made(body: StaticBody2D):
	cargo_ui.toggle_rocket_trail(false)
	body.awaiting_delivery = false
	var delivery_text_instance := delivery_text.instantiate()
	delivery_text_instance.position = body.global_position
	if cargo_ui.get_next_cargo() == body.wanted_cargo:
		main.add_score(30)
		var random_pitch = main.rng.randf_range(-0.05, 0.05)
		$DeliveryBonusSFX.pitch_scale += random_pitch
		$DeliveryBonusSFX.play()
		delivery_text_instance.play_animation(true)
	else:
		main.add_score(10)
		var random_pitch = main.rng.randf_range(-0.05, 0.05)
		$DeliverySFX.pitch_scale += random_pitch
		$DeliverySFX.play()
		delivery_text_instance.play_animation(false)
	combo += 1
	if combo > 1:
		$UI/Countdown/Timer.start(3)
		main.add_score(10 * combo)
		var combo_text_instance := delivery_text.instantiate()
		combo_text_instance.position = ship.global_position
		combo_text_instance.play_animation(2)
		add_child(combo_text_instance)
	cargo_ui.remove_cargo()
	if cargo_ui.get_next_cargo() == CARGO.EMPTY and attempts > 0:
		main.add_score(20 * attempts)
		var attempts_bonus_instance := delivery_text.instantiate()
		attempts_bonus_instance.position = $UI/AttemptsLeftPos.global_position
		attempts_bonus_instance.play_animation(3)
		add_child(attempts_bonus_instance)
	update_score()
	add_child(delivery_text_instance)


func update_score():
	score_text.text = str(main.get_score())


func attempts_left() -> bool:
	$UI/AttemptsBG/Attempts.text = str(attempts)
	if attempts <= 0:
		return false
	else:
		return true


func load_next_level():
	$UI/ShipCountdown.set_visible(false)
	$UI/Countdown.set_visible(false)
	$UI/Countdown/Timer.stop()
	current_level += 1
	attempts = 0
	level_holder.get_child(0).queue_free()
	cargo_ui.queue_free()
	if current_level > levels.size():
		main.ChangeScene(main.ending)
	else:
		# Level setup
		var next_level = levels[current_level-1].instantiate()
		$Aimer.position = next_level.get_node("AimerPlacement").global_position
		aimer.rotation = next_level.get_node("AimerPlacement").global_rotation
		level_holder.add_child(next_level)
		loaded_level = next_level
		
		#setup_ship()
		setup_ship()
		var next_rocket_ui = next_level.rocket_ui.instantiate()
		$UI/Rocket.add_child(next_rocket_ui)
		cargo_ui = next_rocket_ui
		
		for planet in loaded_level.get_node("Planets").get_children():
			attempts += 1
			cargo_ui.add_cargo(planet.wanted_cargo)
		
		attempts -= 1
		$UI/AttemptsBG/Attempts.text = str(attempts)


func setup_ship():
	$UI/AttemptsBG/Attempts.text = str(attempts)
	combo = 0
	$UI/ShipCountdown.set_visible(false)
	$UI/Countdown.set_visible(false)
	$UI/Countdown/Timer.stop()
	$UI/LaunchPad.set_visible(true)
	cargo_ui.toggle_rocket_trail(false)
	launched = false
	var next_ship = loaded_level.ship.instantiate()
	add_child(next_ship)
	ship = next_ship


func start_countdown():
	if ship != null and $UI/Countdown/Timer.is_stopped():
		$UI/ShipCountdown.set_visible(true)
		$UI/Countdown.set_visible(true)
		$UI/Countdown/Timer.start(3)


func _on_timer_timeout():
	$UI/ShipCountdown.set_visible(false)
	$UI/Countdown.set_visible(false)
	$UI/Countdown/Timer.stop()
	if ship != null:
		ship.explode()
