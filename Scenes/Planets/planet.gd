extends StaticBody2D

enum CARGO { EMPTY, RED, BLUE, GREEN }

@export_range(CARGO.EMPTY, CARGO.GREEN, 1) var wanted_cargo = CARGO.EMPTY
@export var awaiting_delivery := true


func _process(delta):
	if awaiting_delivery:
		match wanted_cargo:
			CARGO.RED:
				$GemRed.set_visible(true)
				$GemBlue.set_visible(false)
				$GemGreen.set_visible(false)
			CARGO.BLUE:
				$GemRed.set_visible(false)
				$GemBlue.set_visible(true)
				$GemGreen.set_visible(false)
			CARGO.GREEN:
				$GemRed.set_visible(false)
				$GemBlue.set_visible(false)
				$GemGreen.set_visible(true)
	else:
		$GemRed.set_visible(false)
		$GemBlue.set_visible(false)
		$GemGreen.set_visible(false)
		if name != "Planet":
			$Sprite2D.modulate.r = 0.6
			$Sprite2D.modulate.g = 0.6
			$Sprite2D.modulate.b = 0.6
