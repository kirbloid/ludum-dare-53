extends Control

@onready var main = get_node("/root/Main")
var can_click = false


func _ready():
	update_score()
	match main.current_ending:
		1:
			$Ending1.visible = true


func _process(delta):
	if Input.is_action_just_pressed("action_select") and can_click:
		main.ChangeScene(main.game)
	elif Input.is_action_just_pressed("action_cancel") and can_click:
		main.ChangeScene(main.title_screen)


func update_score():
	$Ending1/ScoreNumber.text = str(main.get_score())


func _on_safe_timeout():
	can_click = true
