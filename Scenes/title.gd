extends Control

@onready var main = get_node("/root/Main")

const MAXINTROS = 2

var screens : Array
var intro = 0


func _ready():
	screens.push_back($TitleScreen)
	screens.push_back($Intro)
	screens.push_back($Instructions)


func _process(delta):
	if Input.is_action_just_pressed("action_select"):
		if intro == MAXINTROS:
			main.ChangeScene(main.game)
		else:
			intro += 1
			screens[intro].show()
	elif Input.is_action_just_pressed("action_cancel"):
		main.ChangeScene(main.game)
