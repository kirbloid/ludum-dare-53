extends Node2D

enum BONUS { REGULAR, DELIVERY_MATCHED, COMBO, ATTEMPTS_LEFT }

func play_animation(bonus: BONUS):
	match bonus:
		BONUS.REGULAR:
			$CenterContainer/Delivery.set_visible(true)
			$AnimationPlayer.play("Delivery")
		BONUS.DELIVERY_MATCHED:
			$CenterContainer/DeliveryBonus.set_visible(true)
			$AnimationPlayer.play("Delivery")
		BONUS.COMBO:
			$CenterContainer/Combo.set_visible(true)
			$AnimationPlayer.play("Delivery")
		BONUS.ATTEMPTS_LEFT:
			$CenterContainer/AttemptsLeft.set_visible(true)
			$AnimationPlayer.play("Delivery")


func _on_animation_player_animation_finished(anim_name):
	queue_free()
