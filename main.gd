extends Control

var title_screen = preload("res://Scenes/title.tscn")
var game = preload("res://Scenes/game.tscn")
var ending = preload("res://Scenes/ending.tscn")

@onready var rng = RandomNumberGenerator.new()

var current_scene
var current_ending = 0
var score : int = 0


func _ready():
	current_scene = title_screen.instantiate()
	ChangeScene(title_screen)
	$MainSong.play()


func ChangeScene(scene):
	current_scene.queue_free()
	current_scene = scene.instantiate()
	add_child(current_scene)


func get_score():
	return score


func reset_score():
	score = 0


func add_score(add: int):
	score += add


func _on_main_song_finished():
	$MainSong.play()
